import 'package:flutter/material.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:ipsi_silat/bottomNavBar.dart';
import 'package:ipsi_silat/loginReg/login.dart';

Future<void> main()async {
  WidgetsFlutterBinding.ensureInitialized();
  var status= false;
    
//final DatabaseService db = DatabaseService();
//db.getVapor();
//db.getCart();
  Map<int, Widget> op = {1: Login(), 2: Login()};

  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CustomSplash(
        imagePath: 'resource/gambarsilat.png',
        backGroundColor: Colors.grey[300],
        animationEffect: 'fade-in',
        logoSize: 100,
        home: status == true ? MainMenu() : Login(),
        duration: 2500,
        type: CustomSplashType.StaticDuration,
        outputAndHome: op,
      ),
    ),
  );
}
