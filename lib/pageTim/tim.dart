import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

class Turnamen extends StatefulWidget {
  @override
  _TurnamenState createState() => _TurnamenState();
}

var tinggi;
var lebar;

class _TurnamenState extends State<Turnamen> {
  @override
  Widget build(BuildContext context) {
    tinggi = MediaQuery.of(context).size.height;
    lebar = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Container(
              margin: EdgeInsets.only(
                  top: tinggi / 15, bottom: tinggi / 50, left: lebar / 30),
              child: Text(
                'List Turnamen',
                style: TextStyle(
                    fontSize: tinggi / lebar * 15,
                    color: Colors.grey[800],
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
                margin: EdgeInsets.only(
                    top: tinggi / 15, bottom: tinggi / 50, right: lebar / 30),
                child: InkWell(
                  onTap: (){},
                    child: Icon(
                  Icons.add,
                  color: Colors.grey[800],
                  size: tinggi / lebar * 15,
                ))),
          ]),
          Container(
            margin: EdgeInsets.only(
                right: lebar / 30, left: lebar / 30, bottom: tinggi / 50),
            child: Theme(
              data: Theme.of(context).copyWith(primaryColor: Colors.grey[800]),
              child: TextField(
                style: TextStyle(fontSize: 13, color: Colors.grey[800]),
                decoration: InputDecoration(
                  hintText: "Cari Turnamen...",
                  suffixIcon: Icon(Icons.search),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[800]),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[800]),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
                onChanged: (String val) {
                  setState(
                    () {
                      val = val;
                    },
                  );
                },
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: lebar / 30),
              child: GridView.count(
                scrollDirection: Axis.vertical,
                childAspectRatio: lebar / tinggi * 2.7,
                crossAxisCount: 1,
                children: List.generate(
                  10,
                  (index) {
                    return Container(
                      child: Container(
                        child: new Center(
                          child: GestureDetector(
                            onTap: () {
                              // Navigator.push(
                              //   context,
                              //   MaterialPageRoute(
                              //       builder: (context) => Details()),
                              // );
                            },
                            child: Stack(
                              children: [
                                Container(
                                  height: 270.0,
                                  width: lebar / 1,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: Colors.transparent,
                                  ),
                                ),
                                Positioned(
                                  bottom: 0.0,
                                  child: Container(
                                    height: tinggi / 4,
                                    width: lebar / 1.072,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                        color: Colors.red[400]),
                                  ),
                                ),
                                Positioned(
                                  right: 2.0,
                                  bottom: 60,
                                  child: Hero(
                                    tag: "Silat Suka Bliat",
                                    child: Container(
                                      height: 170.0,
                                      width: 170.0,
                                      child: Stack(children: [
                                        // FadeInImage.memoryNetwork(
                                        //   placeholder: kTransparentImage,
                                        //   image: "resource/gambarsilat.png",
                                        //   fit: BoxFit.scaleDown,
                                        // ),
                                        Image.asset(
                                          "resource/gambarsilat.png",
                                          fit: BoxFit.contain,
                                        ),
                                      ]),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  top: 80.0,
                                  left: 15.0,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Silat Suka Bliat',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20.0),
                                      ),
                                      SizedBox(
                                        height: tinggi / 8,
                                      ),
                                      Text(
                                        "Magetan",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 27.0),
                                      ),
                                      SizedBox(height: 2.0),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
