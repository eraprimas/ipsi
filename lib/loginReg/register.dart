import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ipsi_silat/bottomNavBar.dart';
import 'package:ipsi_silat/loginReg/login.dart';
import 'package:ipsi_silat/loginReg/register.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

double lebar;
double tinggi;
String nama = "";
String pw = "";
ProgressDialog pr;
var queryParameters;
var url = Uri.https('https://ipsisilat.000webhostapp.com/login.php','', queryParameters);

class _RegisterState extends State<Register> {
  final _formkey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    _showSnackBar() {
      final SnackBar snackBar = new SnackBar(
        content: Text(
          'Email atau password anda salah!',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        duration: Duration(seconds: 3),
        backgroundColor: Colors.red,
        action: SnackBarAction(
            label: 'ok',
            textColor: Colors.white,
            onPressed: () {
              print('njay');
            }),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }

    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      textDirection: TextDirection.rtl,
      isDismissible: false,
    );

    pr.style(
      message: 'Lagi Login',
      borderRadius: 10.0,
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progressWidgetAlignment: Alignment.center,
      progressTextStyle: TextStyle(
          color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
      messageTextStyle: TextStyle(
          color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600),
    );

    // Future<String> signIn(BuildContext context) async {
    //   var res = await http
    //       .get(url, headers: {"Accept": "application/json"});
    //   setState(() {
    //     var resBody = json.decode(res.body);
    //     var status = resBody['status'];
    //     if (status == 'true') {
    //       print(url);
    //       pr.hide();
    //     } else {
    //       _showSnackBar();
    //       pr.hide();
    //     }
    //   });
    // }

    lebar = MediaQuery.of(context).size.width;
    tinggi = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      bottomNavigationBar: Container(
        color: Colors.grey[300],
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(0), topRight: Radius.circular(0)),
          child: Container(
            height: tinggi / 15,
            color: Colors.grey[800],
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Sudah punya akun? ',
                    style: TextStyle(color: Colors.white),
                  ),
                  InkWell(
                    onTap: () async {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Login()));
                    },
                    child: Text(
                      'Login',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      backgroundColor: Colors.grey[300],
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Stack(
                children: [
                  Positioned(
                    child: Container(
                      color: Color(0xFFEB5757),
                      height: tinggi / 2.3,
                      width: double.infinity,
                    ),
                  ),
                  Positioned(
                    child: Center(
                      child: Container(
                        margin: EdgeInsets.only(top: tinggi / 12),
                        alignment: Alignment.center,
                        height: tinggi / 5,
                        width: lebar / 1.5,
                        child: Center(
                          child: Image.asset('resource/gambarsilat.png'),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(
                          lebar / 10, tinggi / 3, lebar / 10, 0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10.0),
                        ),
                        child: Card(
                          margin: EdgeInsets.all(0),
                          elevation: 10,
                          child: Form(
                            key: _formkey,
                            child: Column(
                              children: [
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                      left: lebar / 20, top: tinggi / 50),
                                  child: Text("E-mail"),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: lebar / 20,
                                      vertical: tinggi / 50),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                        primaryColor: Colors.grey[800]),
                                    child: TextFormField(
                                      validator: (nama) => nama.isEmpty
                                          ? 'Masukan E-mail dengan benar'
                                          : null,
                                      style: TextStyle(fontSize: 13),
                                      decoration: InputDecoration(
                                        errorBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        focusedErrorBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.red),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        hintText: "E-Mail",
                                        prefixIcon: Icon(Icons.mail),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey[800]),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                      ),
                                      onChanged: (String val) {
                                        setState(
                                          () {
                                            nama = val;
                                            queryParameters = {
                                              'nama': nama,
                                              'password': pw,
                                            };
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                      left: lebar / 20, bottom: tinggi / 50),
                                  child: Text("Nama"),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      right: lebar / 20,
                                      left: lebar / 20,
                                      bottom: tinggi / 50),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                        primaryColor: Colors.grey[800]),
                                    child: TextFormField(
                                      validator: (pw) => pw.length < 6
                                          ? 'Masukan Nama Anda'
                                          : null,
                                      obscureText: false,
                                      style: TextStyle(fontSize: 13),
                                      decoration: InputDecoration(
                                        errorBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        focusedErrorBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.red),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        hintText: "Nama",
                                        prefixIcon: Icon(Icons.supervised_user_circle),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.red),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                      ),
                                      onChanged: (String val) {
                                        setState(
                                          () {
                                            nama = val;
                                            queryParameters = {
                                              'nama': nama,
                                              'password': pw,
                                            };
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  width: double.infinity,
                                  margin: EdgeInsets.only(
                                      left: lebar / 20, bottom: tinggi / 50),
                                  child: Text("Password"),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      right: lebar / 20,
                                      left: lebar / 20,
                                      bottom: tinggi / 50),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                        primaryColor: Colors.grey[800]),
                                    child: TextFormField(
                                      validator: (pw) => pw.length < 6
                                          ? 'Password harus lebih dari 6 karakter'
                                          : null,
                                      obscureText: true,
                                      style: TextStyle(fontSize: 13),
                                      decoration: InputDecoration(
                                        errorBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        focusedErrorBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.red),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        hintText: "Password",
                                        prefixIcon: Icon(Icons.lock),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.grey),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.red),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                      ),
                                      onChanged: (String val) {
                                        setState(
                                          () {
                                            pw = val;
                                            queryParameters = {
                                              'nama': nama,
                                              'password': pw,
                                            };
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(
                                      right: lebar / 20, bottom: tinggi / 50),
                                  width: double.infinity,
                                  child: Text(
                                    "",
                                    textAlign: TextAlign.right,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    child: Container(
                      height: tinggi / 14,
                      margin: EdgeInsets.only(
                          top: tinggi / 1.22, left: lebar / 9, right: lebar / 9),
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        splashColor: Colors.yellow,
                        color: Color(0xFFF6F578),
                        onPressed: () async {
                          //signIn(context);
                          Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MainMenu(),
                          ),
                          (e) => false);
                        },
                        child: Container(
                          child: Center(
                            child: Text(
                              "Daftar Sekarang",
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
