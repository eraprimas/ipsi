import 'package:flutter/material.dart';
import 'package:ipsi_silat/bracketPainter/bracketPainter.dart';
import 'package:ipsi_silat/pageTim/tim.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

double lebar;
double tinggi;

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    lebar = MediaQuery.of(context).size.width;
    tinggi = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: tinggi * 2.5,
          child: Column(children: [
            Stack(
              children: [
                Positioned(
                  child: Container(
                    height: tinggi / 3,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(100),
                        bottomRight: Radius.circular(100),
                      ),
                      boxShadow: [
                        BoxShadow(color: Color(0xFFEB5757), spreadRadius: 3),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: tinggi / 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: tinggi / 8,
                        child: Image.asset('resource/gambarsilat.png'),
                      ),
                      Container(
                        height: tinggi / 10,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                'IKATAN PENCAK SILAT',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: tinggi / lebar * 8,
                                ),
                              ),
                            ),
                            Container(
                              child: Text(
                                'Silat Elang Putih',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: tinggi / lebar * 7,
                                ),
                              ),
                            ),
                            Container(
                              child: Text(
                                'Cabang Magetan',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300,
                                  fontSize: tinggi / lebar * 7,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: lebar / 10, vertical: tinggi / 50),
                  margin: EdgeInsets.only(
                      top: tinggi / 3.9, left: lebar / 20, right: lebar / 20),
                  width: double.infinity,
                  height: tinggi / 10,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.red.withOpacity(0.3),
                        spreadRadius: 3,
                        blurRadius: 10,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          'Andre Kohar',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        height: tinggi / 10,
                        width: lebar / 500,
                        color: Colors.grey,
                      ),
                      Container(
                        child: Text('Administrator'),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        top: tinggi / 20,
                        bottom: tinggi / 100,
                        left: lebar / 20,
                        right: lebar / 20),
                    child: Text(
                      'Daftar Tim Pencak Silat',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.grey),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      print("anjay");
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Turnamen()),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 3,
                            blurRadius: 10,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(
                          bottom: tinggi / 20,
                          left: lebar / 20,
                          right: lebar / 20),
                      child: Stack(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              height: tinggi / 4,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                              ),
                              child: Image.asset("resource/tim.jpg"),
                            ),
                          ),
                          Container(
                            height: tinggi / 4,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                gradient: LinearGradient(
                                    begin: Alignment.bottomRight,
                                    stops: [
                                      0.1,
                                      0.9
                                    ],
                                    colors: [
                                      Colors.red[300].withOpacity(0.6),
                                      Colors.white.withOpacity(0)
                                    ])),
                          ),
                          Positioned(
                              bottom: tinggi / 50,
                              right: lebar / 30,
                              child: Text(
                                'Lihat Semua Tim',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ))
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: tinggi / 500,
                        bottom: tinggi / 100,
                        left: lebar / 20,
                        right: lebar / 20),
                    child: Text(
                      'Tournament Bracket',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.grey),
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      width: lebar * 10,
                      height: tinggi * 1.5,
                      margin: EdgeInsets.only(
                          bottom: tinggi / 20,
                          left: lebar / 20,
                          right: lebar / 20),
                      child: CustomPaint(
                        painter: BracketPainter(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
