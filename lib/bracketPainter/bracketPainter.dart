import 'package:flutter/material.dart';

class BracketPainter extends CustomPainter {
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0
      ..color = Colors.black;
      var jmlArray = 16;
    double tinggi = 20;
    double tinggi2 = 55;
    double tinggi3 = 120;
    double tinggi4 = 260;
    double tinggi5 = 540;
    
    double x11 = 100;
    double y11 = 45;
    double x21 = 120;
    double y21 = 45;

    double x12 = 100;
    double y12 = 115;
    double x22 = 120;
    double y22 = 115;
    
    double x13 = 120;
    double y13 = 45;
    double x23 = 120;
    double y23 = 115;
    
    double x14 = 150;
    double y14 = 80;
    double x24 = 120;
    double y24 = 80;





    double x112 = 250;
    double y112 = 80;
    double x212 = 265;
    double y212 = 80;

    double x122 = 250;
    double y122 = 220;
    double x222 = 265;
    double y222 = 220;
    
    double x132 = 265;
    double y132 = 80;
    double x232 = 265;
    double y232 = 220;
    
    double x142 = 300;
    double y142 = 145;
    double x242 = 265;
    double y242 = 145;




    double x113 = 400;
    double y113 = 145;
    double x213 = 420;
    double y213 = 145;

    double x123 = 400;
    double y123 = 425;
    double x223 = 420;
    double y223 = 425;
    
    double x133 = 420;
    double y133 = 145;
    double x233 = 420;
    double y233 = 425;
    
    double x143 = 450;
    double y143 = 285;
    double x243 = 420;
    double y243 = 285;





    double x114 = 550;
    double y114 = 285;
    double x214 = 575;
    double y214 = 285;

    double x124 = 550;
    double y124 = 845;
    double x224 = 575;
    double y224 = 845;
    
    double x134 = 575;
    double y134 = 285;
    double x234 = 575;
    double y234 = 845;
    
    double x144 = 575;
    double y144 = 565;
    double x244 = 650;
    double y244 = 565;

    for (var i = 0; i < jmlArray; i++) {
      canvas.drawRRect(
        RRect.fromRectAndRadius(
            Rect.fromLTWH(0, tinggi, 100, 50), Radius.circular(2)),
        paint,
      );
      if (i % 2 == 0) {
        canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(150, tinggi2, 100, 50), Radius.circular(2)),
          paint,
        );

      canvas.drawLine(Offset(x11, y11), Offset(x21, y21), paint);
      canvas.drawLine(Offset(x12, y12), Offset(x22, y22), paint);
      canvas.drawLine(Offset(x13, y13), Offset(x23, y23), paint);
      canvas.drawLine(Offset(x14, y14), Offset(x24, y24), paint);
       y11 +=140;
       y21 +=140;
       y12 +=140;
       y22 +=140;
       y13 +=140;
       y23 +=140;
       y14 +=140;
       y24 +=140;
      }
      if (i % 4 == 0) {
        canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(300, tinggi3, 100, 50), Radius.circular(2)),
          paint,
        );
        canvas.drawLine(Offset(x112, y112), Offset(x212, y212), paint);
      canvas.drawLine(Offset(x122, y122), Offset(x222, y222), paint);
      canvas.drawLine(Offset(x132, y132), Offset(x232, y232), paint);
      canvas.drawLine(Offset(x142, y142), Offset(x242, y242), paint);
       y112 +=280;
       y212 +=280;
       y122 +=280;
       y222 +=280;
       y132 +=280;
       y232 +=280;
       y142 +=280;
       y242 +=280;
      }
      if (i % 8 == 0) {
        canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(450, tinggi4, 100, 50), Radius.circular(2)),
          paint,
        );
        canvas.drawLine(Offset(x113, y113), Offset(x213, y213), paint);
      canvas.drawLine(Offset(x123, y123), Offset(x223, y223), paint);
      canvas.drawLine(Offset(x133, y133), Offset(x233, y233), paint);
      canvas.drawLine(Offset(x143, y143), Offset(x243, y243), paint);
       y113 +=560;
       y213 +=560;
       y123 +=560;
       y223 +=560;
       y133 +=560;
       y233 +=560;
       y143 +=560;
       y243 +=560;
      }
      if (i % 16 == 0 && jmlArray == 16 || i % 16 == 0 && jmlArray > 8) {
        canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(650, tinggi5, 100, 50), Radius.circular(2)),
          paint,
        );
      canvas.drawLine(Offset(x114, y114), Offset(x214, y214), paint);
      canvas.drawLine(Offset(x124, y124), Offset(x224, y224), paint);
      canvas.drawLine(Offset(x134, y134), Offset(x234, y234), paint);
      canvas.drawLine(Offset(x144, y144), Offset(x244, y244), paint);
       y114 +=1120;
       y214 +=1120;
       y124 +=1120;
       y224 +=1120;
       y134 +=1120;
       y234 +=1120;
       y144 +=1120;
       y244 +=1120;
      }
      if (i % 32 == 0 && jmlArray > 16) {
        canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(650, tinggi5, 100, 50), Radius.circular(2)),
          paint,
        );
      }
      tinggi += 70;
      tinggi2 += 70;
      tinggi3 += 70;
      tinggi4 += 70;
    }
    canvas.drawLine(Offset(100.0, 45.0), Offset(120.0, 45.0), paint);
    canvas.drawLine(Offset(100.0, 115.0), Offset(120.0, 115.0), paint);
    canvas.drawLine(Offset(120.0, 45.0), Offset(120.0, 115.0), paint);
    canvas.drawLine(Offset(150.0, 80.0), Offset(120.0, 80.0), paint);
  }

  bool shouldRepaint(BracketPainter oldPainter) => false;
}
